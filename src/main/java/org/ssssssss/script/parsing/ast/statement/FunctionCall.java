package org.ssssssss.script.parsing.ast.statement;

import org.ssssssss.script.compile.MagicScriptCompiler;
import org.ssssssss.script.parsing.Span;
import org.ssssssss.script.parsing.ast.Expression;

import java.util.List;

public class FunctionCall extends Expression {
	private final Expression function;
	private final List<Expression> arguments;

	private final boolean inLinq;

	public FunctionCall(Span span, Expression function, List<Expression> arguments, boolean inLinq) {
		super(span);
		this.function = function;
		this.arguments = arguments;
		this.inLinq = inLinq;
	}

	@Override
	public List<Span> visitSpan() {
		return mergeSpans(function, arguments);
	}

	@Override
	public void visitMethod(MagicScriptCompiler compiler) {
		function.visitMethod(compiler);
		arguments.forEach(it -> it.visitMethod(compiler));
	}

	public Expression getFunction() {
		return function;
	}

	public List<Expression> getArguments() {
		return arguments;
	}

	@Override
	public void compile(MagicScriptCompiler compiler) {
		compiler.visit(function)    // 访问函数
				.load1()    // 参数 MagicScriptContext
				.ldc(getFunction().getSpan().getText())    // 函数名
				.insn(arguments.stream().anyMatch(it -> it instanceof Spread) ? ICONST_1 : ICONST_0)
				.asBoolean();    // 是否有扩展参数(...xxx)
		for (Expression argument : arguments) {
			if (inLinq && argument instanceof MemberAccess) {
				((MemberAccess) argument).compileLinq(compiler);
			} else {
				argument.compile(compiler);
			}
		}
		compiler.call("invoke_function", arguments.size() + 4);    // 调用函数
	}
}